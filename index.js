const express = require('express');
const app = express();

app.get('/', function (req, res) {
  res.send('TechCodeBeer... Life in our Digital World!');
});

app.get('/sport', function (req, res) {
  res.send('I am the new sport page.');
  });

app.get('/anotherRoute', function (req, res) {
  res.send('This is another reply to a different route.');
  });

app.listen(3001, function () {
  console.log('I fixed a fake bug.');
  console.log('Example app listening on port 3001!')
});
